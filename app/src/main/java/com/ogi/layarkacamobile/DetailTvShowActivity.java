package com.ogi.layarkacamobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ogi.layarkacamobile.model.tv.TvShowData;
import com.squareup.picasso.Picasso;

public class DetailTvShowActivity extends AppCompatActivity {

    TextView tvTitle, tvRating, tvOverview, tvRelease;
    ImageView ivPosterPath, ivBackdropPath;
    RatingBar rbRating;

    TvShowData tv_show;
    int tv_show_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tv_show);

        tvTitle = findViewById(R.id.tv_single_title);
        tvRating = findViewById(R.id.tv_single_rating);
        tvOverview = findViewById(R.id.tv_single_overview);
        tvRelease = findViewById(R.id.tv_single_release);
        ivPosterPath = findViewById(R.id.iv_poster_path);
        ivBackdropPath = findViewById(R.id.ivBackdrop);
        rbRating = findViewById(R.id.rb_single_rating);

        setupToolbar();

        Intent mySingleTvShowIntent = getIntent();
        if (mySingleTvShowIntent.hasExtra("tv_shows")) {
            tv_show = getIntent().getParcelableExtra("tv_shows");
            String IMAGE_BASE_URL_BACKDROP = "https://image.tmdb.org/t/p/w780";
            Picasso.get()
                    .load(IMAGE_BASE_URL_BACKDROP +tv_show.getBackdropPath())
                    .placeholder(R.drawable.default_backdrop)
                    .error(R.drawable.broken_image)
                    .into(ivBackdropPath);
            String IMAGE_BASE_URL_POSTER = "https://image.tmdb.org/t/p/w500";
            Picasso.get()
                    .load(IMAGE_BASE_URL_POSTER +tv_show.getPosterPath())
                    .placeholder(R.drawable.default_poster_path)
                    .error(R.drawable.broken_image)
                    .into(ivPosterPath);
            tvTitle.setText(tv_show.getOriginalName());
            tvRelease.setText(tv_show.getFirstAirDate());
            tvRating.setText(String.valueOf(tv_show.getVoteAverage()));
            tvOverview.setText(tv_show.getOverview());
            rbRating.setRating((float) (tv_show.getVoteAverage() / 2));
            tv_show_id = tv_show.getId();
        }
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
